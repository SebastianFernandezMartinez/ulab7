package ulab7;

/**
 * This is the BLT sandwich class that implements the ISandwich interface.
 */
public class BLTSandwich implements ISandwich {
    private String filling;

    /**
     * Constructor: This initializes the filling to "bacon, lettuce, tomato".
     */
    public BLTSandwich() {
        this.filling = "bacon, lettuce, tomato";
    }

    /**
     * @return the filling field
     */
    public String getFilling() {
        return this.filling;
    }

    /**
     * Method that adds a topping to the filling field.
     *
     * @param topping Topping to be added
     */
    @Override
    public void addFilling(String topping) {
        if (!filling.isEmpty()) {
            this.filling += ", ";
        }
        this.filling += topping;
    }

    /**
     * @return false always
     */
    public boolean isVegetarian() {
        return false;
    }
}
