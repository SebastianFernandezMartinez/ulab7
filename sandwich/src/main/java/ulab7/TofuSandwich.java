package ulab7;;

public class TofuSandwich extends VegetarianSandwich{

    public TofuSandwich() {
        this.addFilling("Tofu");
    }

    @Override
    public String getProtein() {
        return "Tofu";
    }
}