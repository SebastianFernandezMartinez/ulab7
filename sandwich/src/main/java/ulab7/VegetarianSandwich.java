package ulab7;

/**
 * this is the vegetarian sandwich class that implements the ISandwich interface
 */
public abstract class VegetarianSandwich implements ISandwich {
    private String filling;

    /**
     * constructor: this intialises the filling to an empty String
     */
    public VegetarianSandwich(){
        this.filling = "";
    }
    
    public String getFilling(){
        return this.filling;
    }

    public abstract String getProtein();

    /**
     * method that adds a topping to the filling feild
     * @param topping topping that should be added
     */
    public void addFilling(String topping) throws IllegalArgumentException {
            
            if(topping.contains("chicken") ||
                topping.contains("beef") ||
                topping.contains("fish") ||
                topping.contains("meat") ||
                topping.contains("pork")){

                throw new IllegalArgumentException("cannot add meat");

            }
            if(this.filling.equals("")){
                this.filling = topping;
            }
            else{
                this.filling = this.filling + ", " + topping;
            }
        }
    

        /**
         * method that tests if the sandwich respects the vegetarian rules
         * @return boolean that says if it is or not
         */
    public boolean isVegetarian(){
        return true;
    }
    public boolean isVegan(){
        if(this.filling.contains("egg") || (this.filling.contains("cheese"))){
            return false;
        }else{
            return true;
        }
    }
}

