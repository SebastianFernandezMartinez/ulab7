package ulab7;

public class CaesarSandwich extends VegetarianSandwich{
    private String filling;

    public CaesarSandwich(){
        this.filling = "Caeser dressing";
        
    }
    @Override
    public void addFilling(String topping) throws IllegalArgumentException {
        String[] notAllowed = {"chicken","beef","fish","meat","pork"};

        for(int i=0;i<notAllowed.length;i++) {
            if(topping.equals(notAllowed[i])){
                throw new IllegalArgumentException("this is a veggie sandwich");
            }
        }
        this.filling += ", "+topping;
    }
    @Override
    public String getFilling() {
        // TODO Auto-generated method stub
        return this.filling;
    }
    @Override
    public String getProtein() {
        return "Anchovies";
    }
    @Override
    public boolean isVegetarian() {
        return false;
    }

}
