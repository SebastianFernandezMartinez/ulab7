package ulab7;

import static org.junit.Assert.*;

import org.junit.Test;

public class VegetarianSandwichTest {

    @Test
    public void testAddFilling() {
        TofuSandwich sandwich = new TofuSandwich();
        sandwich.addFilling("lettuce");
        String filling = sandwich.getFilling();
        assertEquals("Tofu, lettuce", filling); // Updated expected filling
    }

    @Test
    public void testGetFilling() {
        VegetarianSandwich sandwich = new TofuSandwich();
        assertEquals("Tofu", sandwich.getFilling());
    }

    @Test
    //vegan and vegetarian s
    public void testIsVeganandVegetarian() {
        TofuSandwich sandwich = new TofuSandwich();
        TofuSandwich sandwich2 = new TofuSandwich();
        TofuSandwich sandwich3 = new TofuSandwich();

        sandwich.addFilling("cheese");
        sandwich2.addFilling("egg");
        sandwich3.addFilling("lettuce");

        assertFalse(sandwich.isVegan());
        assertTrue(sandwich2.isVegetarian());
        assertTrue(sandwich3.isVegetarian());
    }
}
