package ulab7;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * testing class to test the BLTSandwich class
 */

public class BLTSandwichTest {
    
    /**
     * test the constructor and get methods
     */
    @Test
    public void TestConstructorBLTSandwich(){
        BLTSandwich blt = new BLTSandwich();

        assertEquals("bacon, lettuce, tomato", blt.getFilling());
    }

    /**
     * testing the addFilling method if it truly add topping to the filling
     * feild separated by a comma and a space
     */
    @Test
    public void testAddFilling(){
        BLTSandwich blt = new BLTSandwich();
        String oldToppingList = "bacon, lettuce, tomato";
        String newTopping = "cheese";
        blt.addFilling(newTopping);
        String expectedFilling = oldToppingList + (oldToppingList.isEmpty() ? "" : ", ") + newTopping;
        assertEquals(expectedFilling, blt.getFilling());
    }
    
    /**
     * testing the IsVegan method to see if it always returns false
     * 
     */
    @Test
    public void TestIsVegetarian(){
        BLTSandwich blt = new BLTSandwich();

        assertTrue(!blt.isVegetarian());
    }
}
